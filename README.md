# Java Internship Technical assignment
Below you'll find 3 exercises of increasing complexity.  
Please, do as much as you can.  
We expect your solutions to be published on Github/GitLab/Bitbucket

# Exercise 1

## Birthday Cake Candles
You are in charge of the cake for a child's birthday. You have decided the cake will have one candle for each year of their total age. They will only be able to blow out the tallest of the candles. Count how many candles are tallest. 

### Example
$`candles=[4,4,3,1]`$  
The maximum height candles are $`4`$ units high. There are $`2`$ of them, so return $`2`$.

### Function Description
Write the function *birthdayCakeCandles* in any programming language you know.

#### Input Parameters
*birthdayCakeCandles* has the following input parameter:
1. int $`candles[n]`$: the candle heights

#### Returns
1. int: the number of candles that are tallest

#### Input Format
The first line contains a single integer, $`n`$, the size of $`candles[]`$.
The second line contains $`n`$ space-separated integers, where each integer $`i`$ describes the height of $`candles[i]`$.

#### Constrains
1. $`1 <= n <= 10^3`$
2. $`1 <= candles[i] <= 10^3`$

#### Sample Input
> 4  
> 3 2 1 3

#### Sample Output
> 2
# Exercise 2

## Task Manager
You need to create a java application that will work as a task manager, with this application we should be able to do:
1. create new users (insert: FirstName, LastName, userName)
2. show all users (prin: FirstName, LastName, number of tasks)
3. add a task to the user (insert username, Task Title, Description)
4. show user's tasks (print: Task title, Description)
All data should be kept in the file, writing and reading should happen via serialization and deserilization operations. 

### Acceptance Criteria 
1. Create new users - by running this command:   
> java -jar myaplication.jar -createUser -fn='FirstName' - ln='LastName' -un='UserName'
UserName should be unique,  dot'n forget about validation
2. Show All Users - by running this command:  
> java -jar myaplication.jar -showAllUsers
4. Add a task to the user - by running this command:  
> java -jar myaplication.jar -addTask -un='userName' -tt='Task Title' -td='Task Description'
5. Show user's tasks - by running this command:  
> java -jar myaplication.jar -showTasks -un='userName'

# Exercise 3

## Enhanced Task Manager
Add additional functionality to the application that you'll have   created:
1. Add one more logical functionality (e. g. task assigned to a group of users 
2. Use an Automated Build Tool (Maven/Gradle)
3. Use Database instead of storing data in a file
4. Cover your code with JUnit tests
